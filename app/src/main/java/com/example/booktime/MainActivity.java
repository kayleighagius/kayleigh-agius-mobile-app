package com.example.booktime;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;

    private FirebaseAuth mAuth;
    private Button mReadBtn;
    private Button mToReadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mReadBtn = (Button) findViewById(R.id.alrReadBtn);
        mToReadBtn = (Button) findViewById(R.id.toReadBtn);

        mReadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alreadyRead = new Intent(MainActivity.this, AlreadyReadActivity.class);
                startActivity(alreadyRead);
            }
        });

        mToReadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toRead = new Intent(MainActivity.this, ToReadActivity.class);
                startActivity(toRead);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_recent_activity:
                Intent recentAct = new Intent(MainActivity.this, RecentActivity.class);
                startActivity(recentAct);
                break;
            case R.id.nav_friends:
                Intent friendsAct = new Intent(MainActivity.this, YourFriends.class);
                startActivity(friendsAct);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            return true;
        } else if (item.getItemId() == R.id.action_logout_btn) {
            logOut();
        }
        else if(item.getItemId() == R.id.searchBook){
            Intent searchAct = new Intent(MainActivity.this, SearchActivity.class);
            startActivity(searchAct);
        }

        return super.onOptionsItemSelected(item);
    }

   private void logOut() {
       LoginManager.getInstance().logOut();
       mAuth.signOut();
       finish();
       Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
       startActivity(loginIntent);
   }
}