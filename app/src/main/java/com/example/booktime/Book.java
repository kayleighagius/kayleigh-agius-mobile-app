package com.example.booktime;

public class Book {
 //   private String imageUrl;
    private String title;
    private String author;
    private String subtitle;
    private String pubDate;

    public Book(String title, String author, String subtitle, String pubDate) {
        this.title = title;
        this.author = author;
       // this.imageUrl = imageUrl;
        this.subtitle = subtitle;
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getPubDate(){
        return pubDate;
    }
}

