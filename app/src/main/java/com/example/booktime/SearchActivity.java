package com.example.booktime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private BookAdapter mBookAdapter;
    private ArrayList<Book> mBookList = new ArrayList<>();;
    private RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRequestQueue = Volley.newRequestQueue(this);
        parseJSON();

    }


    private void parseJSON(){
        String url = "https://www.googleapis.com/books/v1/volumes?q=search+terms";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("items");

                    for(int i=0; i<jsonArray.length(); i++){
                        String volumes = jsonArray.getJSONObject(i).getString("volumeInfo");
                        JSONObject item = new JSONObject(volumes);

                        String bookTitle = "";
                        String authorName = "";
                        String subtitle = "";
                        String pubDate = "";
                        if(!item.isNull("title") ){
                            bookTitle = item.getString("title");
                        }
                        if(!item.isNull("authors") ){
                            authorName = item.getString("authors");
                        }
                        if(!item.isNull("subtitle") ){
                            subtitle = item.getString("subtitle");
                        }
                        if(!item.isNull("publishedDate") ){
                            pubDate = item.getString("publishedDate");
                        }


                        mBookList.add(new Book(bookTitle, authorName, subtitle, pubDate));
                    }

                    mBookAdapter = new BookAdapter(SearchActivity.this, mBookList);
                    mRecyclerView.setAdapter(mBookAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(request);
    }


}
