package com.example.booktime;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.common.internal.service.Common;

import java.util.ArrayList;


public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    private Context mContext;
    private ArrayList<Book> mBookList;

    public BookAdapter(Context context, ArrayList<Book> bookList){
        mContext = context;
        mBookList = bookList;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.book_item, viewGroup, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BookViewHolder bookViewHolder, int position) {
        Book currentBook = mBookList.get(position);

        String bookName = currentBook.getTitle();
        String authorName = currentBook.getAuthor();
        String subtitle = currentBook.getSubtitle();
        String pubDate = String.valueOf(currentBook.getPubDate());

        bookViewHolder.mTextViewBookTitle.setText(bookName);
        bookViewHolder.mTextViewAuthor.setText(authorName);
        bookViewHolder.mTextViewSubtitle.setText(subtitle);
        bookViewHolder.mTextViewDate.setText(pubDate);

    }

    @Override
    public int getItemCount() {
        return mBookList.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder{

        public TextView mTextViewBookTitle;
        public TextView mTextViewAuthor;
        public TextView mTextViewSubtitle;
        public TextView mTextViewDate;
        public ImageView mAddToAlreadyRead;
        public ImageView mAddToToRead;

        public BookViewHolder(@NonNull View bookView) {
            super(bookView);

            mTextViewBookTitle = bookView.findViewById(R.id.text_view_title);
            mTextViewAuthor = bookView.findViewById(R.id.text_view_author);
            mTextViewSubtitle = bookView.findViewById(R.id.text_view_subtitle);
            mTextViewDate = bookView.findViewById(R.id.text_view_date);
            mAddToAlreadyRead = (ImageView) bookView.findViewById(R.id.addToRead);
            mAddToToRead = (ImageView) bookView.findViewById(R.id.addToToRead);
        }
    }
}
